(function() {
    "use strict";
        var quiz = {
        "name": "Quiz",
        "description": "Ilu pseudonimów znasz?",
        "question": "Jakie jest pradziwe imię ",
        "questions": [
            {"question": "Supermana", "answer": "Clarke Kent", "asked": false },
            {"question": "Batmana", "answer": "Bruce Wayne", "asked": false },
            {"question": "Snoop Dogga", "answer": "Calvin Cordozar Broadus Junior", "asked": false },
            {"question": "Frediego Mercurego", "answer": "Farrokh Bulsara", "asked": false },
            {"question": "Twórcy tej strony", "answer": "Marek Pomianowski", "asked": false },
            {"question": "Wonder Woman", "answer": "Dianna Prince", "asked": false },
            {"question": "Bolesława Prusa", "answer": "Aleksander Głowacki", "asked": false },
            {"question": "Czesława Niemena", "answer": "Czesław Wydrzycki", "asked": false },
            {"question": "Marilyn Monroe", "answer": "Norma Jeane Mortensen", "asked": false },
            {"question": "Marka Twaina", "answer": "Samuel Langhorne Clemens", "asked": false },
            {"question": "Lady Gagi", "answer": "Stefani Joanne Angelina Germanotta", "asked": false }
        ]
    };

    //// dom references ////
    var $question = document.getElementById("question");
    var $score = document.getElementById("score");
    var $feedback = document.getElementById("feedback");
    var $start = document.getElementById("start");
    var $form = document.getElementById("answer");
    var $timer = document.getElementById("timer");
    var $restart = document.getElementById("restart");

    /// view functions ///

    function update(element,content,klass) {
        var p = element.firstChild || document.createElement("p");
        p.textContent = content;
        element.appendChild(p);
        if(klass) {
            p.className = klass;
        }
    }

    function hide(element) {
        element.style.display = "none";
    }

    function show(element) {
        element.style.display = "block";
    }

    // Event listeners
    $start.addEventListener('click', function() { new Game(quiz) } , false);

    $restart.addEventListener('click', function () {location.reload();}, false);

    // hide the form at the start of the game
    hide($form);

    //// function definitions ////

    function random(a,b,callback) {
        if(b===undefined) {
            // if only one argument is supplied, assume the lower limit is 1
            b = a, a = 1;
        }
        var result = Math.floor((b-a+1) * Math.random()) + a;
        if(typeof callback === "function") {
            result = callback(result);
        }
        return result;
    }

    function Game(quiz){
        this.questions = quiz.questions;
        this.phrase = quiz.question;
        this.score = 0; // initialize score
        update($score,this.score);
        // initialize time and set up an interval that counts down every second
        this.time = 40;
        this.questionsAsked = 0;
        update($timer,this.time);
        this.interval = window.setInterval( this.countDown.bind(this) , 1000 );
        // hide button and show form
        hide($start);
        show($form);
        // add event listener to form for when it's submitted
        $form.addEventListener('click', function(event) {
                this.questionsAsked++;
                event.preventDefault();
                this.check(event.target.value);
        }.bind(this), false);
        this.chooseQuestion();
    }

    // Method definitions
    Game.prototype.chooseQuestion = function() {
        console.log("chooseQuestion() called");
        var questions = this.questions.filter(function(question){
            return question.asked === false;
        });
        // set the current question
        this.question = questions[random(questions.length) - 1];
        if(this.questionsAsked < this.questions.length) {
            this.ask(this.question);
        }
    };

    Game.prototype.ask = function(question) {
        console.log("ask() called");

        var quiz = this;
        // set the question.asked property to true so it's not asked again
        question.asked = true;
        update($question,this.phrase + question.question + "?");
        // clear the previous options
        $form.innerHTML = "";
        // create an array to put the different options in and a button variable
        var options = [], button;
        var option1 = chooseOption();
        options.push(option1.answer);
        var option2 = chooseOption();
        options.push(option2.answer);
        var option3 = chooseOption();
        options.push(option3.answer);
        // add the actual answer at a random place in the options array
        options.splice(random(0,3),0,this.question.answer);
        // loop through each option and display it as a button
        options.forEach(function(name) {
            button = document.createElement("button");
            button.value = name;
            button.textContent = name;
            $form.appendChild(button);
        });

        // choose an option from all the possible answers but without choosing the answer or the same option twice
        function chooseOption() {
            var option = quiz.questions[random(quiz.questions.length) - 1];
            // check to see if the option chosen is the current question or already one of the options, if it is then recursively call this function until it isn't
            if(option === question || options.indexOf(option.answer) !== -1) {
                return chooseOption();
            }
            return option;
        }

    };

    Game.prototype.check = function(answer) {
        console.log("check() called");
        if(answer === this.question.answer){
            update($feedback,"Super!","right");
            // increase score by 1
            this.score++;
            update($score,this.score);
            $($feedback).animate({opacity: 1});
            setTimeout(function(){$($feedback).animate({opacity: 0})}, 500);
        } else {
            update($feedback,"Źle!","wrong");
            $($feedback).animate({opacity: 1});
            setTimeout(function(){$($feedback).animate({opacity: 0})}, 500);
        }
        this.chooseQuestion();
    };

    Game.prototype.countDown = function() {
        // this is called every second and decreases the time
        // decrease time by 1
        this.time--;
        // update the time displayed
        update($timer,this.time);
        // the game is over if the timer has reached 0
        if(this.time <= 0 || this.questionsAsked >= this.questions.length) {
            this.gameOver();
            hide($feedback);
        }
    };

    Game.prototype.gameOver = function() {
        console.log("gameOver() invoked");
        // inform the player that the game has finished and tell them how many points they have scored
        update($question, "Koniec gry, liczba punktów: " + this.score );
        // stop the countdown interval
        window.clearInterval(this.interval);
        hide($form);
        show($restart);
    }
}());